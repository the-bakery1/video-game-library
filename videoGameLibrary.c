#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    char name[50];
    char studio[50];
    char genre[50];
} videoGame;

int printMainMenu();
void viewLibrary(videoGame* library, int size);

// Program to have an inventory of video games.
// Video game includes name, studio, genre.
int main()
{
    // Print menu, user selects option.
    // 1 - View all names.
    // 2 - Add game.
    // 3 - Remove game.
    // 4 - Filter games.

    videoGame myFirstGame = {"TF2", "Valve", "Shooter"};
    videoGame myLibary[1] = { myFirstGame };

    int choice = printMainMenu();

    if(choice == 1)
    {
        int size = sizeof(myLibary) / sizeof(videoGame);
        viewLibrary(myLibary, size);
    }
    else if (choice == 2)
    {
        // Add
        printf("Unimplemented!");
    }
    else if (choice == 3)
    {
        // Remove
        printf("Unimplemented!");
    }
    else
    {
        // Wrong input!
        printf("Unimplemented!");
    }
    
    return 0;
}

int printMainMenu()
{
    int choice = 0;
    printf("Please select an option...\n"
           "1. View all games.\n"
           "2. Add a game.\n"
           "3. Remove a game.\n");
    
    scanf("%d", &choice);
    return choice;
}

void viewLibrary(videoGame* library, int size)
{
    printf("\nAll games:\n");
    for(int i = 0; i < size; i++)
    {
        printf("%d.\t%s\n", i + 1, library[i].name);
    }
}
